package ex02;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.file.FileProps;
import io.vertx.core.file.FileSystem;

public class FolderSearchVerticle extends AbstractVerticle {

	private final String path;
	private final long depth;

	public FolderSearchVerticle(String path, long depth) {
		super();
		this.path = path;
		this.depth = depth;
	}

	@Override
	public void start(final Future<Void> done) {
		this.launchSearch(handler -> {
			if (handler.succeeded()) {
				done.complete();
			}
		});
	}

	private void launchSearch(Handler<AsyncResult<Void>> handler) {
		Future<Void> future = Future.<Void>future().setHandler(handler);
		this.asyncSearch(path, depth, future);
	}

	@SuppressWarnings("rawtypes")
	private void asyncSearch(String path, long depth, Future<Void> future) {
		if (depth >= 0) {
			final FileSystem fs = vertx.fileSystem();
			fs.exists(path, existsHandler -> {
				existsHandler.failed();
				if (existsHandler.failed()) {
					future.fail(existsHandler.cause());
				} else {
					fs.lprops(path, propsHandler -> {
						if (propsHandler.failed()) {
							future.fail(propsHandler.cause());
						} else {
							final FileProps props = propsHandler.result();
							if (props.isDirectory()) {
								fs.readDir(path, readHandler -> {
									if (readHandler.result() != null) {
										List<Future> futures = new ArrayList<>();
										readHandler.result().forEach(document -> {
											Future<Void> fut = Future.future();
											futures.add(fut);
											this.asyncSearch(document, depth-1, fut);											
										});
										
										CompositeFuture.all(futures).setHandler(h -> {
											if(h.succeeded()) {
												future.complete();
											}
										});
									}
								});
							} else if (props.isRegularFile()) {
								vertx.eventBus().send(Launcher02.MESSAGE_NEWFILE, path);	
								
								MessageConsumer<Boolean> consumer = vertx.eventBus().consumer(path);
								consumer.handler(res -> {
									if (res.body()) {
										future.complete();
									}
								});
							}
						}
					});
				}
			});
		} else {
			future.complete();
		}

	}

}
