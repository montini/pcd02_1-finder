package ex02;

import java.util.stream.IntStream;

import gui.InfoGUI;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;
import logic.AbstractLauncher;
import logic.RegexMatcher;

public class Launcher02 extends AbstractLauncher {
	
	public final static String MESSAGE_NEWFILE = "newFile";
	public final static String MESSAGE_KILL = "kill";

	public Launcher02(String path, String regex, int depth) {
		super(path, regex, depth, new RegexMatcher());
	}

	@Override
	protected void computeJob() {
		Vertx vertx = Vertx.vertx();
		
		int nDocumentVerticles = Runtime.getRuntime().availableProcessors();
		IntStream.range(0, nDocumentVerticles)
		.forEach(i -> vertx.deployVerticle(new DocumentVerticle(super.regexMatcher, super.regex)));

		Verticle folderVert = new FolderSearchVerticle(super.path, super.depth);
		vertx.deployVerticle(folderVert, res -> {
			if (res.succeeded()) {
				InfoGUI.disableButtons(false);
				cron.stop();
				InfoGUI.elapsedTime(cron.getTime());
				
				vertx.undeploy(res.result());
				IntStream.range(0, nDocumentVerticles)
				.forEach(i -> vertx.eventBus().send(MESSAGE_KILL, true));
			}
		});
	}

}

