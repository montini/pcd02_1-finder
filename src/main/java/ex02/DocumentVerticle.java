package ex02;

import java.io.File;
import java.io.IOException;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Verticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import logic.RegexMatcher;
import model.Document;

public class DocumentVerticle extends AbstractVerticle implements Verticle {

	private RegexMatcher regexMatcher;
	private String regex;

	public DocumentVerticle(RegexMatcher regexMatcher, String regex) {
		super();
		this.regexMatcher = regexMatcher;
		this.regex = regex;
	}

	@Override
	public void start(Future<Void> future) {	
		EventBus eb = vertx.eventBus();
		MessageConsumer<String> fileConsumer = eb.consumer(Launcher02.MESSAGE_NEWFILE);
		fileConsumer.handler(message -> {
			vertx.executeBlocking(executed -> {
				Document document;
				try {
					document = Document.fromFile(new File(message.body()));
					this.regexMatcher.occurrencesCount(document, regex);
					executed.complete();
				} catch (IOException e) {}
			}, executed -> {
				eb.send(message.body(),true);
			});
		});
		MessageConsumer<String> killerConsumer = eb.consumer(Launcher02.MESSAGE_KILL);
		killerConsumer.handler(handler -> {
			vertx.undeploy(this.deploymentID());
		});
	}

}
