package model;

import java.io.File;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.LinkedList;
import java.util.List;

public class Folder {
	private final List<Folder> subFolders;
	private final List<Document> documents;
	private final File orig;

	public Folder(List<Folder> subFolders, List<Document> documents, File orig) {
		this.subFolders = subFolders;
		this.documents = documents;
		this.orig = orig;
	}

	public List<Folder> getSubFolders() {
		return this.subFolders;
	}

	public List<Document> getDocuments() {
		return this.documents;
	}

	public String getName() {
		return this.orig.getName();
	}

	public static Folder fromDirectory(File dir, long depth) throws IOException {
		List<Document> documents = new LinkedList<Document>();
		List<Folder> subFolders = new LinkedList<Folder>();
		depth--;
		for (File entry : dir.listFiles()) {
			if (depth >= 0) {
				if (entry.isDirectory()) {
					try {
						subFolders.add(Folder.fromDirectory(entry, depth));
					} catch (NullPointerException e) {

					}
				} else {
					try {
						documents.add(Document.fromFile(entry));
					} catch (AccessDeniedException e1) {}
				}
			}

		}
		return new Folder(subFolders, documents, dir);
	}

}

