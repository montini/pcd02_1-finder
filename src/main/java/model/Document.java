package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.List;

public class Document {

	private final List<String> lines = new ArrayList<>();
	private final File orig;
    
    public Document(File orig) {
        this.orig = orig;
    }
    
    public List<String> getLines() {
    	if (this.lines.isEmpty()) {
    	        BufferedReader reader;
    	        try {
    	        	reader = new BufferedReader(new FileReader(this.orig));
    	            String line = reader.readLine();
    	            while (line != null) {
    	                this.lines.add(line);
    	                line = reader.readLine();
    	            }
    	        } catch (IOException ex){} 
    	}
        return this.lines;
    }
    
    public String getName() {
    	return this.orig.getPath();
    }
    
    public static Document fromFile(File file) throws IOException, AccessDeniedException {
    		if (!file.canRead() && !file.isDirectory()) {
    			throw new AccessDeniedException("File cannot be read");
    		}
        return new Document(file);
    }
}
