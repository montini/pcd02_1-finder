package ex03;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import gui.InfoGUI;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import logic.AbstractLauncher;
import logic.RegexMatcher;
import model.Document;

public class Launcher03 extends AbstractLauncher {

	public Launcher03(String path, String regex, int depth) {
		super(path, regex, depth, new RegexMatcher());
	}

	@Override
	protected void computeJob() {
		int nThreads = Runtime.getRuntime().availableProcessors() + 1;
		ExecutorService executor = Executors.newFixedThreadPool(nThreads);
		Scheduler scheduler = Schedulers.from(executor);

		Observable.create(source -> {
			Files.walk(Paths.get(path), super.depth, FileVisitOption.FOLLOW_LINKS).filter(p -> Files.isRegularFile(p))
			.parallel().forEach(f -> {
				source.onNext(f.toString());
			});
			source.onComplete();
		})		
		.flatMap(i -> 
			Observable.just(i).subscribeOn(scheduler).map(onNext -> {
				try {
					Document document = Document.fromFile(new File(onNext.toString()));
					super.regexMatcher.occurrencesCount(document, super.regex);
				} catch (IOException e) {}
				return onNext;
			})
		).doFinally(() -> {
			cron.stop();
			InfoGUI.disableButtons(false);
			InfoGUI.elapsedTime(cron.getTime());
			executor.shutdown();
		}).subscribe();
		
	}

}