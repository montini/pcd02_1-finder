package ex01;

import java.util.concurrent.RecursiveTask;

import logic.RegexMatcher;
import model.Document;

public class DocumentTask extends RecursiveTask<Void> {
    
	private static final long serialVersionUID = 1L;
	private final Document document;
    private final String regex;
    private final RegexMatcher regexMatcher;
    
    public DocumentTask(RegexMatcher regexMatcher, Document document, String regex) {
        super();
        this.document = document;
        this.regex = regex;
        this.regexMatcher = regexMatcher;
    }
    
    @Override
    protected Void compute() {
        regexMatcher.occurrencesCount(document, regex);
        return null;
    }
}