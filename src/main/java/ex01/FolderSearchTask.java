package ex01;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import logic.RegexMatcher;
import model.Document;
import model.Folder;

public class FolderSearchTask extends RecursiveTask<Void> {

	private static final long serialVersionUID = 1L;
	private final Folder folder;
	private final String regex;
	private final RegexMatcher regexMatcher;
	
	public FolderSearchTask(RegexMatcher regexMatcher, Folder folder, String searchedWord) {
		super();
		this.regexMatcher = regexMatcher;
		this.folder = folder;
		this.regex = searchedWord;
	}

	@Override
	protected Void compute() {
		List<RecursiveTask<Void>> forks = new LinkedList<>();
		for (Folder subFolder : folder.getSubFolders()) {
			FolderSearchTask task = new FolderSearchTask(regexMatcher, subFolder, regex);
			forks.add(task);
			task.fork();
		}

		for (Document document : folder.getDocuments()) {
			DocumentTask task = new DocumentTask(regexMatcher, document, regex);
			forks.add(task);
			task.fork();
		}

		for (RecursiveTask<Void> task : forks) {
			task.join();
		}
		return null;
	}
}
