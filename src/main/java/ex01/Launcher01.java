package ex01;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ForkJoinPool;

import gui.InfoGUI;
import logic.AbstractLauncher;
import logic.RegexMatcher;
import model.Folder;

public class Launcher01 extends AbstractLauncher {  
	
	public Launcher01(String path, String regex, int depth) {
		super(path, regex, depth, new RegexMatcher());
	}

	@Override
	protected void computeJob() {
		try {
			super.folder = Folder.fromDirectory(new File(path), depth);
		} catch (IOException e1) { e1.printStackTrace(); }
		
		ForkJoinPool folderForkJoin = new ForkJoinPool(8);
		folderForkJoin.invoke(new FolderSearchTask(super.regexMatcher, super.folder, super.regex));
		cron.stop();
		InfoGUI.disableButtons(false);
		InfoGUI.elapsedTime(cron.getTime());
	}
}
