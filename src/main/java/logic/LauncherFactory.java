package logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import ex01.Launcher01;
import ex02.Launcher02;
import ex03.Launcher03;

public class LauncherFactory {
	private String path;
	private String regex;
	private int depth;
	
	public final static int CONTROLLER01 = 1;
	public final static int CONTROLLER02 = 3;
	public final static int CONTROLLER03 = 2;
	
	public LauncherFactory setPath(String path) throws FileNotFoundException {
		this.path = path;
		if (!Files.exists(new File(this.path).toPath())) {
			throw new FileNotFoundException();
		}
		return this;
	}
	
	public LauncherFactory setRegex(String regex) {
		this.regex = regex;
		return this;
	}
	
	public LauncherFactory setDepth(int depth) {
		this.depth = depth;
		return this;
	}
	
	public AbstractLauncher buildController(int code) {
		AbstractLauncher controller = null;
		switch(code) {
		case CONTROLLER01:
			controller = new Launcher01(path, regex, depth);
			break;
		case CONTROLLER02:
			controller = new Launcher02(path, regex, depth);
			break;
		case CONTROLLER03:
			controller = new Launcher03(path, regex, depth);
			break;		
		}
		return controller;
	}
	

}
