package logic;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import gui.ListItem;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DataManager {

	public static ObservableList<ListItem> obsList = FXCollections.synchronizedObservableList(FXCollections.observableArrayList());
	private static int sum = 0;
	private static int fileNotMatched = 0;

	public static DoubleProperty mean = new SimpleDoubleProperty(0);
	private double myMean;

	public static DoubleProperty perc = new SimpleDoubleProperty(0);
	private double myPerc;

	public synchronized void updateData(String documentName, long count) {
		int len = obsList.size();
		if (count>0) {
			sum += count;
			this.myMean = roundTwoDigits((double) sum / (len + 1));
		} else {
			fileNotMatched++;
			this.myPerc = roundTwoDigits((double) len / (len + fileNotMatched)*100);
		}
		
		final CountDownLatch doneLatch = new CountDownLatch(1);
		Platform.runLater(() -> {
			if (count > 0) {
				obsList.add(new ListItem(documentName, count));
			}
			mean.set(myMean);
			perc.set(myPerc);
			doneLatch.countDown();
		});
		try {
			doneLatch.await(3500,TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {}
	}

	public static void reset() {
		sum = 0;
		fileNotMatched = 0;
		Platform.runLater(() -> {
			DataManager.obsList.clear();
			DataManager.mean.set(0);
			DataManager.perc.set(0);
		});	
	}

	private double roundTwoDigits(double val) {
		return (double) Math.round(val*100)/100;
	}
}
