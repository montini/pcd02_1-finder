package logic;

import java.io.IOException;

import javafx.application.Application;
import gui.InfoGUI;

public class Main {

	public static void main(String[] args) throws IOException {
		Application.launch(InfoGUI.class);
	}

}
