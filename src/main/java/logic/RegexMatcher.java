package logic;

import model.Document;

public class RegexMatcher {    

	private final DataManager dataManager = new DataManager();
	
	public String[] wordsIn(String line) {
		return line.trim().split("(\\s|\\p{Punct})+");
	}

	public Long occurrencesCount(Document document, String regex) {
		long count = 0;
		for (String line : document.getLines()) {
			for (String word : wordsIn(line)) {
				if (word.matches(regex)) {
					count++;
				}
			}
		}
		
		this.dataManager.updateData(document.getName(), count);

		return count;
	}
}
