package logic;

import model.Folder;

public abstract class AbstractLauncher {

	protected final String path;
	protected final String regex;
	protected final int depth;

	protected Folder folder;
	protected final RegexMatcher regexMatcher;

	protected final Cron cron = new Cron();

	public AbstractLauncher(String path, String regex, int depth, RegexMatcher regexMatcher) {
		this.path = path;
		this.regex = regex;
		this.depth = depth;
		this.regexMatcher = regexMatcher;

		cron.start();	
		computeJob();

	}

	protected abstract void computeJob();
}
