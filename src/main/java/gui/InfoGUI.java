package gui;

import java.io.FileNotFoundException;
import java.util.Arrays;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import logic.DataManager;
import logic.LauncherFactory;

public class InfoGUI extends Application implements EventHandler<ActionEvent> {

	private static final String DEFAULT_PATH = "D:\\Documenti\\Dropbox";
	private static final String DEFAULT_REGEX = "[a-zA-Z]{8}";
	private static final String DEFAULT_DEPTH = "2";

	private final static Button start01Button = new Button("Start 01");
	private final static Button start02Button = new Button("Start 02");
	private final static Button start03Button = new Button("Start 03");

	private final TextField pathTxt = new TextField();
	private final TextField regexTxt = new TextField();
	private final TextField depthTxt = new TextField();

	private TableView<ListItem> mainTable = new TableView<>();

	private Label percLbl = new Label();
	private Label meanLbl = new Label();

	private final LauncherFactory launcherFactory = new LauncherFactory();

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Regex Finder Outputs");
		VBox mainBox = new VBox(20);
		mainBox.setAlignment(Pos.CENTER);

		GridPane setupGrid = new GridPane();
		setupGrid.setHgap(10);
		setupGrid.setVgap(10);
		setupGrid.setAlignment(Pos.CENTER);


		pathTxt.setText(DEFAULT_PATH);
		pathTxt.setOnMouseClicked(e -> {
			DirectoryChooser directoryChooser = new DirectoryChooser();
			try {
				String path = directoryChooser.showDialog(primaryStage).getAbsolutePath();
				pathTxt.setText(path);
			} catch (NullPointerException ex) {}			
		});
		regexTxt.setText(DEFAULT_REGEX);
		depthTxt.setText(DEFAULT_DEPTH);
		depthTxt.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, 
					String newValue) {
				if (!newValue.matches("\\d*")) {
					depthTxt.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});


		setupGrid.add(centeredLabel("Folder"), 1, 1);
		setupGrid.add(pathTxt, 1, 2);

		setupGrid.add(centeredLabel("Regex"), 2, 1);
		setupGrid.add(regexTxt, 2, 2);

		setupGrid.add(centeredLabel("Depth"), 3, 1);
		setupGrid.add(depthTxt, 3, 2);


		HBox startBox = new HBox(20);
		startBox.setAlignment(Pos.CENTER);

		start01Button.setOnAction(this);
		start02Button.setOnAction(this);
		start03Button.setOnAction(this);

		meanLbl.textProperty().bind(DataManager.mean.asString());
		percLbl.textProperty().bind(DataManager.perc.asString().concat("%"));
		startBox.getChildren().addAll(start01Button, start02Button, start03Button);
		startBox.getChildren().addAll(new Separator(Orientation.VERTICAL), new Label("Files with matches:"), percLbl, new Label("Match per file:"), meanLbl);

		TableColumn<ListItem, String> col1 = new TableColumn<>("Path");
		TableColumn<ListItem, Long> col2 = new TableColumn<>("Count");

		col1.setCellValueFactory(new PropertyValueFactory<ListItem, String>("Path"));
		col2.setCellValueFactory(new PropertyValueFactory<ListItem, Long>("Count"));

		col1.prefWidthProperty().bind(mainTable.widthProperty().multiply(0.9));
		col2.prefWidthProperty().bind(mainTable.widthProperty().multiply(0.1));

		this.mainTable.getColumns().addAll(Arrays.asList(col1, col2));
		this.mainTable.setItems(DataManager.obsList);		
		this.mainTable.setColumnResizePolicy(p -> true);

		mainBox.getChildren().addAll(setupGrid, new Separator(), startBox, new Separator(), this.mainTable);
		Scene scene = new Scene(mainBox, 1000, 700);
		primaryStage.setScene(scene);
		primaryStage.show();

		primaryStage.setOnCloseRequest(e -> {
			Platform.exit();
			System.exit(0);
		});
	}

	@Override
	public void handle(ActionEvent ev) {
		new Thread(() -> {
			DataManager.reset();
			Object src = ev.getSource();
			try {
				this.launcherFactory.setPath(pathTxt.getText()).setRegex(regexTxt.getText()).setDepth(new Integer(depthTxt.getText()));
				disableButtons(true);
				if (src.equals(start01Button)) {
					this.launcherFactory.buildController(LauncherFactory.CONTROLLER01);
				} else if (src.equals(start02Button)) {
					this.launcherFactory.buildController(LauncherFactory.CONTROLLER02);
				} else if (src.equals(start03Button)) {
					this.launcherFactory.buildController(LauncherFactory.CONTROLLER03);
				}
			} catch (FileNotFoundException e) {
				Platform.runLater(()-> {
					Alert alert = new Alert(AlertType.ERROR, "Please select a valid folder", ButtonType.OK);
					alert.showAndWait();
				});
				
				System.out.println("File not found");
			}

		}).start();	
	}

	private Label centeredLabel(String text) {
		Label lbl = new Label(text);
		GridPane.setHalignment(lbl, HPos.CENTER);
		return lbl;
	}

	public static void disableButtons(boolean disabled) {
		start01Button.setDisable(disabled);
		start02Button.setDisable(disabled);
		start03Button.setDisable(disabled);
	}

	public static void elapsedTime(long millis) {
		System.out.println(millis);
		Platform.runLater(() -> {
			//Alert alert = new Alert(AlertType.NONE, "Execution Ended!\nElapsed time:" + millis + "ms", ButtonType.OK);
			//alert.showAndWait();
		});
	}

}
