package gui;

public class ListItem {
	
	private final String path;
	private final long count;
	
	public ListItem(String path, long count) {
		super();
		this.path = path;
		this.count = count;
	}

	public String getPath() {
		return path;
	}

	public long getCount() {
		return count;
	}

}
